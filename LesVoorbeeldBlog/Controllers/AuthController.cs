using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using LesVoorbeeldBlog.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LesVoorbeeldBlog.Controllers
{
    
    public class AuthController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;

        public AuthController(SignInManager<IdentityUser> signInManager)
        {
            _signInManager = signInManager;
        }

        [HttpPost("/api/auth/login")]
        public async Task<IActionResult> Login([FromBody]CredentialModel model)
        {
            if(!ModelState.IsValid)
                return UnprocessableEntity(ModelState);

            var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, false, false);
            if (result.Succeeded)
            {
                return Ok();
            }

            return BadRequest("Failed to login");
        }
    }

    public class CredentialModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}