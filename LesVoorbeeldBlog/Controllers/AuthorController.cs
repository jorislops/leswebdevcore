using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using LesVoorbeeldBlog.Models;
using LesVoorbeeldBlog.Repository;
using LesVoorbeeldBlog.Services;
using LesVoorbeeldBlog.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace LesVoorbeeldBlog.Controllers
{

//    [EnableCors("AnyGET")]
    [ApiController]
    [Route("api/[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly DbContext _db;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<AuthController> _logger;
        
        private readonly IMapper _mapper;
        
        private readonly IAuthorRepository _authorRepository;
        private readonly IBlacklistService _blackListService;

        public AuthorController(BlogContext db, 
            UserManager<IdentityUser> userManager,
            ILogger<AuthController> logger,
            IMapper mapper,
            IMessageRepository messageRepository, 
            IAuthorRepository authorRepository,
            IBlacklistService blackListService)
        {
            _db = db;
            _userManager = userManager;
            _logger = logger;
            _mapper = mapper;
            _blackListService = blackListService;
            _authorRepository = authorRepository;
        }

        [HttpGet(Name = nameof(GetAuthor))]
        public ActionResult<IEnumerable<AuthorVM>> GetAuthor()
        {
//            var authorDtos = _db.Authors
//                .Include(x => x.Owner)
//                .Select(x => AuthorVM.From(x)).ToList();

//            var authorRepository = new AuthorRepository(_db);
            var authors = _authorRepository.Get();
            var authorsVms = _mapper.Map<IEnumerable<AuthorVM>>(authors);
                //.Select(x => AuthorDto.From(x));
            
            return Ok(authorsVms);
        }
        
        [HttpGet("{id}")]
        public ActionResult<AuthorVM> GetAuthors(int id)
        {
//            var author = _db.Authors
//                .Include(x => x.Owner)
//                .SingleOrDefault(x => x.Id == id);
            
            
            var author = _authorRepository.GetByIdWithOwner(id);
            
            if (author == null)
            {
                return NotFound();
            }

            //not with a mapper
            //_mapper.Map<AuthorVM>(author);
            return Ok(AuthorVM.From(author));
        }
        
        

//        [EnableCors("FromPostTest.com")]
        [Authorize]
        [HttpPost()]
        public IActionResult CreateAuthor([FromBody] AuthorCreationVm author)
        {
            var entityToSave = author.ToAuthor();

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }
            
            var user = _userManager.FindByNameAsync(this.User.Identity.Name).Result;
            if (user == null)
            {
                return Unauthorized();
            }
            
            entityToSave.Owner = user;
            
            //_db.Authors.Add(entityToSave);
            _authorRepository.Insert(entityToSave);

            if (_db.SaveChanges() == 0)
            {
                _logger.LogCritical("Cannot Save Author");
                throw new Exception("Author creation failed at Save");
            }
            
            _logger.LogInformation($"Author {entityToSave.Id} create by user: {user.UserName}");
            
            return CreatedAtRoute(nameof(GetAuthor), 
                new {id = entityToSave.Id}, 
                AuthorVM.From(entityToSave));
        }

//        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult DeleteAuthor(int id)
        {
            //Author author = _db.Authors.FirstOrDefault(x => x.Id == id);
            Author author = _authorRepository.GetById(id);
            if (author == null)
                return NotFound();

            //_db.Authors.Remove(author);
            _authorRepository.Delete(author);
            
            if(_db.SaveChanges() == 0)
                throw new Exception("Author deletion failed at Save");

            return NoContent();
        }

//        [Authorize]
        [HttpPut("{id}")]
        public IActionResult UpdateAuthor(int id, [FromBody] AuthorUpdateVm author)
        {
            if (author == null)
                return BadRequest();

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }

            //Author authorToUpdate = _db.Authors.Find(id);
            Author authorToUpdate = _authorRepository.GetById(id);
            if (authorToUpdate == null)
                return NotFound();
            
            author.UpdateAuthor(authorToUpdate);

            if (_db.SaveChanges() == 0)
            {
                throw new Exception("Author update failed at Save");
            }

            return NoContent();
        }

//        [Authorize]
        [HttpPatch("{id}")]
        public IActionResult PartiallyUpdateAuthor(int id, [FromBody] JsonPatchDocument<AuthorUpdateVm> patch)
        {
            if (patch == null)
                return BadRequest();

            //Author auhtorFromRepo = _db.Authors.SingleOrDefault(x => x.Id == id);
            Author auhtorFromRepo = _authorRepository.GetById(id);
            if (auhtorFromRepo == null)
                return NotFound();

            AuthorUpdateVm authorToPatch = AuthorUpdateVm.From(auhtorFromRepo);
            patch.ApplyTo(authorToPatch, ModelState);

            TryValidateModel(authorToPatch);

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }
            
            authorToPatch.UpdateAuthor(auhtorFromRepo);

            if (_db.SaveChanges() == 0)
            {
                throw new Exception("Partially update author failed on save.");
            }

            return NoContent();
        }
        
        [HttpGet("[action]/{id}")]
        public ActionResult<bool> BlackListAuthor(int id)
        {
            bool blackList = false;
            try
            {
                blackList = _blackListService.BlacklistAuthor(id);
            }
            catch (Exception)
            {
                return NotFound(id);
            }
            
            return Ok(blackList); 
        }
        
        [HttpOptions]
        public IActionResult GetAuthorsOptions()
        {
            Response.Headers.Add("Allow", "GET,OPTIONS,POST,PUT,PATCH");
            return Ok();
        }
    }
}