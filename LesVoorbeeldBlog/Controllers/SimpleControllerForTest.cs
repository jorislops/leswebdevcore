using System.Collections.Generic;
using LesVoorbeeldBlog.Repository;
using LesVoorbeeldBlog.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace LesVoorbeeldBlog.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SimpleControllerForTest : ControllerBase
    {
        private readonly IAuthorRepository _authorRepository;

        public SimpleControllerForTest(IAuthorRepository authorRepository)
        {
            _authorRepository = authorRepository;
        }

        [HttpGet("{id}")]
        public ActionResult<AuthorVM> Get(int id)
        {
            var author = _authorRepository.GetByIdWithOwner(id);
            
            if (author == null)
            {
                return NotFound();
            }

            return Ok(AuthorVM.From(author));
        }
    }
}