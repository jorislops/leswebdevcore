using System;

namespace LesVoorbeeldBlog.Extensions
{
    public static class DateTimeExtension
    {
        public static int GetAge(this DateTime dateTime)
        {
            var currentDate = DateTime.UtcNow;
            int age = currentDate.Year - dateTime.Year;

            if (currentDate < dateTime.AddYears(age))
            {
                age--;
            }

            return age;
        }
    }
}