using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LesVoorbeeldBlog.Repository;
using Microsoft.AspNetCore.Identity;

namespace LesVoorbeeldBlog.Models
{
    public class Author
    {
        public int Id { get; set; }
        //public int AuthorId { get; set; }
        [Required, MinLength(2), MaxLength(255)]
        public string FirstName { get; set; }
        [Required, MinLength(2), MaxLength(255)]
        public string LastName { get; set; }
        
        [Required]
        public string Email { get; set; }
        
        public DateTime BirthDate { get; set; }
        public IdentityUser Owner { get; set; }

        public ICollection<Message> Messages { get; set; }
    }
}