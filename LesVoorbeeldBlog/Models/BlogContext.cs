using LesVoorbeeldBlog.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LesVoorbeeldBlog.Models
{
    public class BlogContext : IdentityDbContext
    {
        public DbSet<Message> Messages { get; set; }
        public DbSet<Author> Authors { get; set; }
        
//        public DbSet<IdentityUser> IdentityUsers { get; set; }
//        public DbSet<IdentityUserRole<string>> IdentityRoles { get; set; }

        public BlogContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }

//    public class UnitOfWork
//    {
//        private readonly BlogContext _blogContext;
//        public UnitOfWork(BlogContext blogContext)
//        {
//            _blogContext = blogContext;
//        }
//        
//        public MessageRepository MessageRepository { get; set; }
//        public AuthorRepository AuthorRepository { get; set; }
//
//
//        public bool Commit()
//        {
//            _blogContext.SaveChanges();
//        }
//    }
}