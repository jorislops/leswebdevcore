using System;
using System.ComponentModel.DataAnnotations;
using LesVoorbeeldBlog.Repository;

namespace LesVoorbeeldBlog.Models
{
    public class Message
    {
        public int Id { get; set; }
        //public int MessageId { get; set; }
        [Required, MinLength(2), MaxLength(255)]
        public string Subject { get; set; }
        [Required]
        public string Description { get; set; }
        
        public DateTime PostDateTime { get; set; }
        
        public int AuthorId { get; set; }
        public Author Author { get; set; }
    }
}