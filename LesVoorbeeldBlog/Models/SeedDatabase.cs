using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Bogus;
using Microsoft.AspNetCore.Identity;

namespace LesVoorbeeldBlog.Models
{
    public class SeedDatabase
    {
        public static void Seed(BlogContext blogContext, UserManager<IdentityUser> userManager, 
            RoleManager<IdentityRole> roleManager)
        {
            var adminUser = new IdentityUser()
            {
                UserName = "Admin",
                Email = "admin@admin.com"
            };

            var userCreatedResult = userManager.CreateAsync(adminUser, "Test@123!").Result;
            
            var role = new IdentityRole("Admin");
            var roleResult = roleManager.CreateAsync(role).Result;
            var addRoleResult = userManager.AddToRoleAsync(adminUser, "Admin").Result;

            if (!userCreatedResult.Succeeded || !roleResult.Succeeded || !addRoleResult.Succeeded)
            {
                throw new InvalidOperationException("Failed to build user and roles");
            }
            
            Randomizer.Seed = new Random(8675309);

            var messageFaker = new Faker<Message>()
                .RuleFor(message => message.Subject, (faker, message) => faker.Lorem.Sentences(1))
                .RuleFor(message => message.Description, (faker, message) => faker.Lorem.Paragraph())
                .RuleFor(message => message.PostDateTime, (faker, message) => faker.Date.Past());

            var authorFaker = new Faker<Author>()
                .RuleFor(author => author.FirstName, faker => faker.Person.FirstName)
                .RuleFor(author => author.LastName, faker => faker.Person.LastName)
                .RuleFor(author => author.Email, faker => faker.Person.Email)
                .RuleFor(author => author.Owner, _ => adminUser)
                .RuleFor(author => author.BirthDate, faker => faker.Date
                    .Between(new DateTime(1920, 1, 1), new DateTime(2000, 1, 1) ))
                .RuleFor(author => author.Messages, (faker, author) =>
                {
                    var messages = messageFaker.Generate(faker.Random.Int(5, 10)).ToList();

                    //add f**k for blacklisting
                    Enumerable.Range(1, 4).ToList().ForEach(
                        x => faker.PickRandom(messages).Description += "f**k"     
                    );
                    
                    return messages;
                });

            var authors = authorFaker.Generate(50);
            blogContext.Authors.AddRange(authors);

            blogContext.SaveChanges();
        }
    }
}