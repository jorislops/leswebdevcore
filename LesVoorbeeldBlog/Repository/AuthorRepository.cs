using System.Linq;
using LesVoorbeeldBlog.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace LesVoorbeeldBlog.Repository
{
    public class AuthorRepository : Repository<Author>, IAuthorRepository
    {
        public AuthorRepository(BlogContext dbContext) : base(dbContext)
        {
        }

        public Author GetByIdWithOwner(int id)
        {
            return AsQueryable().Include(x => x.Owner)
                .FirstOrDefault(x => x.Id == id);
        }
    }
}