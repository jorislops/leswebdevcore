using LesVoorbeeldBlog.Models;

namespace LesVoorbeeldBlog.Repository
{
    public interface IAuthorRepository : IRepository<Author>
    {
        Author GetByIdWithOwner(int id);
    }
}