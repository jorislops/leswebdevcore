using System.Collections.Generic;
using LesVoorbeeldBlog.Models;

namespace LesVoorbeeldBlog.Repository
{
    public interface IMessageRepository : IRepository<Message>
    {
        IEnumerable<Message> GetMessageForAuthor(Author author);
    }
}