using System.Collections.Generic;
using System.Linq;
using LesVoorbeeldBlog.Models;

namespace LesVoorbeeldBlog.Repository
{
    public class MessageRepository : Repository<Message>, IMessageRepository
    {
        private BlogContext _db;

        public MessageRepository(BlogContext db) : base(db)
        {
            _db = db;
        }
        
        public IEnumerable<Message> GetMessageForAuthor(Author author)
        {
            return _db.Messages.Where(x => x.Author == author);
        }
        
    }
}