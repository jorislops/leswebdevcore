using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LesVoorbeeldBlog.Models;
using Microsoft.EntityFrameworkCore;

namespace LesVoorbeeldBlog.Repository
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DbContext _dbContext;

        //Normally Repository works only with UnitOfWork!
        //this is only for demonstration purpose, such that this repository
        //also works without a UnitOfWork.  

        public Repository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        //exposes IQueryable interface to outside world (with a public access modifier)
        //is not a good idea! 
        protected IQueryable<T> AsQueryable()
        {
            return _dbContext.Set<T>().AsNoTracking();
        }

        public virtual T GetById(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }

        public virtual IEnumerable<T> Get()
        {
            return _dbContext.Set<T>().AsEnumerable();
        }

        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return _dbContext.Set<T>()
                .Where(predicate)
                .AsEnumerable();
        }

        public void Insert(T entity)
        {
            _dbContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
        }
    }
}