using System;
using LesVoorbeeldBlog.Models;
using LesVoorbeeldBlog.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace LesVoorbeeldBlog.Services
{
    public class BlacklistService : IBlacklistService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IMessageRepository _messageRepository;
        private readonly BlogContext _db;
        private ILogger<BlacklistService> _logger;

        public BlacklistService(
            IAuthorRepository authorRepository, 
            IMessageRepository messageRepository,  
            BlogContext db,
            ILogger<BlacklistService> logger) 
        {
            _authorRepository = authorRepository;
            _messageRepository = messageRepository;
            _db = db;
            _logger = logger;
        }
        
        public bool BlacklistAuthor(int id)
        {
            bool blackList = false;

            var author = _authorRepository.GetById(id);
            if (author == null)
                throw new Exception("Author not found!");
            
            foreach (var message in _messageRepository.GetMessageForAuthor(author))
            {
                if (message.Description.Contains("f**k"))
                {
                    //message is not really deleted
                    //message are delete when SaveChanges() is called!
                    _messageRepository.Delete(message);
                    //suppose something goes wrong with deleting or program crashes (Exception)!
                    //not one message is deleted (all or nothing behaviour, like a transaction).
                    //throw new Exception();
                   
                    blackList = true;
                }
            }

            if (blackList)
            {
                
                _db.SaveChanges();
                _logger.Log(LogLevel.Information, $"Deleted Message for Author: {author.LastName}, {author.FirstName}");
            }

            return blackList;
        }
    }
}