namespace LesVoorbeeldBlog.Services
{
    public interface IBlacklistService
    {
        bool BlacklistAuthor(int id);
    }
}