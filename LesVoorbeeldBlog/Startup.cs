﻿using System.Threading.Tasks;
using AutoMapper;
using LesVoorbeeldBlog.Models;
using LesVoorbeeldBlog.Repository;
using LesVoorbeeldBlog.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Swashbuckle.AspNetCore.Swagger;

namespace LesVoorbeeldBlog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BlogContext>(options => 
                options.UseSqlite("Data Source=Blog.db"));
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<BlogContext>();

            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<IMessageRepository, MessageRepository>();

            services.AddTransient<IBlacklistService, BlacklistService>();

            services.AddAuthentication()
                .AddCookie(options => { 
                    //event are not working as I expected
                    options.Events.OnRedirectToLogin = context =>
                    {
                        if (context.Request.Path.StartsWithSegments("/api") && context.Response.StatusCode == 200)
                        {
                            context.Response.StatusCode = 401;
                        }
                        return Task.CompletedTask;
                    };
                    options.Events.OnRedirectToAccessDenied = context =>
                    {
                        if (context.Request.Path.StartsWithSegments("/api") && context.Response.StatusCode == 200)
                        {
                            context.Response.StatusCode = 403;
                        }
                        return Task.CompletedTask;
                    };
                });

            services.AddAutoMapper();
            
            //enable cors
//            services.AddCors();
            
            //enable cors with policies
            services.AddCors(options =>
            {
                options.AddPolicy("FromPostTest.com", builder =>
                {
                    builder
                        .WithOrigins("http://www.test.com")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
                options.AddPolicy("FromEveryWhereGet", builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .WithMethods("GET")
                        .AllowAnyOrigin();
                });
            });

            services.AddSwaggerGen(options =>
                options.SwaggerDoc("v1", new Info() {Title = "LesExample", Version = "v1"}));
            
            //logging is configured in appsettings....json file
            //is a service!
//            services.AddLogging(builder =>
//            {
//                //builder.AddConsole();
//            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, BlogContext blogContext, 
            UserManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager)
        {
            app.UseSwagger();
                
            blogContext.Database.EnsureDeleted();
            blogContext.Database.EnsureCreated();
            
            SeedDatabase.Seed(blogContext, signInManager, roleManager);

//            app.UseCors(builder => builder
//                .AllowAnyHeader()
//                .AllowAnyMethod()
//                .AllowAnyOrigin()
//                //.WithOrigins("http://localhost:8080/")
//            );
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //https://localhost:5001/swagger/index.html 
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Les Voorbeeldblog API V1");
            });


            
            app.UseAuthentication();
            
            //app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}