using AutoMapper;
using LesVoorbeeldBlog.Extensions;
using LesVoorbeeldBlog.Models;

namespace LesVoorbeeldBlog.ViewModels
{
    public class AuthorAutoMapperProfile : Profile
    {
        public AuthorAutoMapperProfile()
        {
            CreateMap<Author, AuthorVM>()
                .ForMember(x => x.Age, x => x.MapFrom(author => author.BirthDate.GetAge()))
                .ForMember(x => x.Name, x => x.MapFrom(author => $"{author.FirstName} {author.LastName}"))
                .ForMember(x => x.OwnerName, x => x.MapFrom(author => author.Owner.UserName));


//            public static AuthorDto From(Author author)
//            {
//                return new AuthorDto()
//                {
//                    Id = author.Id,
//                    Name = $"{author.FirstName} {author.LastName}",
//                    Age = author.BirthDate.GetAge(),
//                    Email = author.Email,
//                    OwnerName = author?.Owner?.UserName
//                };
//            }
        }
    }
}