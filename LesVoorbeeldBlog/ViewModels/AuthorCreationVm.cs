using System;
using System.ComponentModel.DataAnnotations;
using LesVoorbeeldBlog.Models;

namespace LesVoorbeeldBlog.ViewModels
{
    public class AuthorCreationVm
    {
        [Required, MinLength(2), MaxLength(255)]
        public string FirstName { get; set; }
        [Required, MinLength(2), MaxLength(255)]
        public string LastName { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }
        
        public DateTime BirthDate { get; set; }
//        public ICollection<Message> Messages = new List<Message>();
        public Author ToAuthor()
        {
            return new Author()
            {
                FirstName = FirstName,
                LastName = LastName,
                BirthDate = BirthDate,
                Email = Email
            };
        }
    }
}