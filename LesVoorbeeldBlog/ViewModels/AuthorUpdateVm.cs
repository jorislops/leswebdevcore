using LesVoorbeeldBlog.Models;

namespace LesVoorbeeldBlog.ViewModels
{
    public class AuthorUpdateVm : AuthorCreationVm
    {
        public void UpdateAuthor(Author author)
        {
            author.Email = Email;
            author.FirstName = FirstName;
            author.LastName = LastName;
            author.BirthDate = BirthDate;
        }

        public static AuthorUpdateVm From(Author author)
        {
            return new AuthorUpdateVm()
            {
                Email = author.Email,
                FirstName = author.FirstName,
                LastName = author.LastName,
                BirthDate = author.BirthDate
            };
        }
    }
}