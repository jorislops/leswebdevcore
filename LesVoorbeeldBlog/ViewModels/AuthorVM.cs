using LesVoorbeeldBlog.Extensions;
using LesVoorbeeldBlog.Models;

namespace LesVoorbeeldBlog.ViewModels
{
    public class AuthorVM
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public int Age { get; set; }

        public string Email { get; set; }
        
        public string OwnerName { get; set; }
        
        public static AuthorVM From(Author author)
        {
            return new AuthorVM()
            {
                Id = author.Id,
                Name = $"{author.FirstName} {author.LastName}",
                Age = author.BirthDate.GetAge(),
                Email = author.Email,
                OwnerName = author?.Owner?.UserName
            };
        }
    }
}