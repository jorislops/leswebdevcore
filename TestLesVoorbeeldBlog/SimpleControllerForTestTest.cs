using System;
using LesVoorbeeldBlog.Controllers;
using LesVoorbeeldBlog.Models;
using LesVoorbeeldBlog.Repository;
using LesVoorbeeldBlog.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace TestLesVoorbeeldBlog
{
    public class SimpleControllerForTestTest
    {
        [Fact]
        public void Get_ReturnsAuthorVm_VMModelHasCorrectAuthorValues()
        {
            // Arrange
            var mockRepo = new Mock<IAuthorRepository>();
            var author = new Author()
            {
                Id = 1, 
                BirthDate = new DateTime(2000, 1, 1),
                Email = "test@test.com",
                FirstName = "test1 firstname",
                LastName = "test1 lastname",
                Owner = new IdentityUser("AdminTest")
            };
            mockRepo.Setup(repo => repo.GetByIdWithOwner(1))
                .Returns(author);
            
            var controller = new SimpleControllerForTest(mockRepo.Object);

            // Act
            var result = controller.Get(1);

            var okObjectResult = Assert.IsAssignableFrom<OkObjectResult>(result.Result);
            var authorVm = Assert.IsAssignableFrom<AuthorVM>(okObjectResult.Value); 

            // Assert
            Assert.Equal(author.Id, authorVm.Id);
            Assert.Equal(author.FirstName + " " +author.LastName, authorVm.Name);
            Assert.Equal(author.Owner.UserName, authorVm.OwnerName);
        }
    }
}